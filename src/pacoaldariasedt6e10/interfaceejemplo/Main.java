
package pacoaldariasedt6e10.interfaceejemplo;

public class Main {


    public static void main(String[] args) {
        
        IMonitor monitor;
        
        monitor= new MonitorLG();
        monitor.mostrarPunto();
        
        monitor= new MonitorSamsung();        
        monitor.mostrarPunto();
    }
    
}
/* Ejecucion
Monitor LG
Monitor Samsung
*/