/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt6e10.claseabstracta;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public abstract class Profesor {
   
   protected float sueldo;
   
   public Profesor() { sueldo=1000; }
   abstract public double getSueldo ();
    
}
