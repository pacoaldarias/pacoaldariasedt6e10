/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt6e10.claseabstracta;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ProfesorTitular pt =  new ProfesorTitular();
        ProfesorInterino pi =  new ProfesorInterino();
        System.out.println("Sueldo PT "+ pt.getSueldo());
        System.out.println("Sueldo PI "+ pi.getSueldo());
    }
    
}
/* Ejecucipn
Sueldo PT 1000.0
Sueldo PI 900.0
*/